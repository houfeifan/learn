package com.learn.multithread.threadLocal;

import com.carrotsearch.sizeof.RamUsageEstimator;
import org.junit.Test;
import org.openjdk.jol.info.ClassLayout;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MemoryLeakTest {
    final static ThreadPoolExecutor poolExecutor = new ThreadPoolExecutor(5, 5, 1, TimeUnit.MINUTES, new LinkedBlockingQueue<>());
    final static ThreadLocal<LocalVariable> localVariable = new ThreadLocal<>();

    public static void main(String[] args) throws InterruptedException {
        for (int i = 0; i < 50; ++i) {
            poolExecutor.execute(new Runnable() {
                @Override
                public void run() {
                    localVariable.set(new LocalVariable());
                    System.out.println("use local variable");
                    localVariable.remove();
                }
            });
            Thread.sleep(1000);
        }
    }

    @Test
    public void showMemorySize() {
        System.out.println("pool execute over");
        System.out.println(RamUsageEstimator.sizeOf(new LocalVariable()));
        System.out.println("0 : " + RamUsageEstimator.sizeOf(new Byte[0]));
        System.out.println("1 : " + RamUsageEstimator.sizeOf(new Byte[1]));
        System.out.println("2 : " + RamUsageEstimator.sizeOf(new Byte[2]));
        System.out.println("3 : " + RamUsageEstimator.sizeOf(new Byte[3]));
        System.out.println("4 : " + RamUsageEstimator.sizeOf(new Byte[4]));

        System.out.println("---------------------LONG------------------");
        System.out.println("0 : " + RamUsageEstimator.sizeOf(new Long[0]));
        System.out.println("1 : " + RamUsageEstimator.sizeOf(new Long[1]));
        System.out.println("2 : " + RamUsageEstimator.sizeOf(new Long[2]));
        System.out.println("3 : " + RamUsageEstimator.sizeOf(new Long[3]));
        System.out.println("4 : " + RamUsageEstimator.sizeOf(new Long[4]));
        System.out.println("5 : " + RamUsageEstimator.sizeOf(Long.class));
    }

    @Test
    public void showObjectScheme() {
        System.out.println(ClassLayout.parseInstance(new Long[0]).toPrintable());
        System.out.println(ClassLayout.parseInstance(new Long[1]).toPrintable());
        System.out.println(ClassLayout.parseInstance(new Long[2]).toPrintable());
        System.out.println(ClassLayout.parseInstance(new Long[3]).toPrintable());
        long[] longs = new long[2];
        longs[0] = 2;
        longs[1] = 3;
        System.out.println(ClassLayout.parseInstance(longs).toPrintable());
        System.out.println(ClassLayout.parseInstance(longs).instanceSize());
        System.out.println(ClassLayout.parseInstance(new Long[2]).instanceSize());

    }

    static class LocalVariable {
        // TODO？ Byte 和 byte 执行结果不同
        //private byte[] a = new byte[1024 * 1024 * 10];
        private Byte[] a = new Byte[1024 * 1024 * 10];
    }

}
