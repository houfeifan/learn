package com.learn.multithread.threadLocal;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequiredArgsConstructor
public class ThreadLocalTest {
    private static final ThreadLocal<Integer> currentUser = ThreadLocal.withInitial(() -> null);

    @GetMapping("wrong")
    public Map wrong(@RequestParam("userId") Integer userId) {
        Map result = getResult(userId);
        return result;
    }

    private Map getResult(Integer userId) {
        String before = Thread.currentThread().getName() + ":" + currentUser.get();
        currentUser.set(userId);
        String after = Thread.currentThread().getName() + ":" + currentUser.get();
        Map result = new HashMap(2);
        result.put("before", before);
        result.put("after", after);
        return result;
    }

    @GetMapping("success")
    public Map success(@RequestParam("userId") Integer userId) {
        try {
            return getResult(userId);
        } finally {
            currentUser.remove();
        }
    }
}
