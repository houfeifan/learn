package com.learn.experiment;

import lombok.extern.slf4j.Slf4j;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author shaun
 * @since 2022/1/19 4:29 下午
 **/
@Slf4j
@SpringBootTest
@RunWith(SpringRunner.class)
public class Junit4Integrate {

}
